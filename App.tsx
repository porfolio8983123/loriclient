import { View, Text, StyleSheet, StatusBar } from 'react-native'
import React from 'react';
import BookRide from './src/screens/BookRide';
import {PaperProvider} from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AvailableRides from './src/screens/AvailableRides';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
      <NavigationContainer>
        <PaperProvider>
            <StatusBar backgroundColor="#0B1124" barStyle="light-content"/>
            <Stack.Navigator initialRouteName='BookingDetails'>
              <Stack.Screen name = "BookingDetails" component={BookRide}
                options={{
                  headerShown: false
                }}
              />

              <Stack.Screen
                name='AvailableRides'
                component={AvailableRides}
              />
            </Stack.Navigator>
        </PaperProvider>
      </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
      backgroundColor:'#0B1124',
      flex:1
  }
})


export default App