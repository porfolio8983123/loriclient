import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import React, { useState } from 'react';
import BookingHeader from '../components/BookingHeader';
import { TextInput, RadioButton,Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/AntDesign';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useNavigation } from '@react-navigation/native';

const BookRide = () => {

  const navigation = useNavigation();

  const [isDatePickerVisible,setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible,setTimePickerVisibility] = useState(false);
  const [selectedDate,setSelectedDate] = useState(null);
  const [selectedTime,setSelectedTime] = useState(null); 

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  }

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  }

  const handleDateConfirm = (date) => {
    setSelectedDate(date);
    console.log(date)
    hideDatePicker();
  }

  const showTimePicker = () => {
    setTimePickerVisibility(true);
  }

  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  }

  const handleTimeConfirm = (time) => {
    setSelectedTime(time);
    console.log(time);
    hideTimePicker();
  }

  const formatTime = (time:Date) => {
    return time.toLocaleTimeString('en-US',{
      hour:'numeric',
      minute:'numeric',
      hour12:true
    })
  }

  return (
    <View style = {{backgroundColor:'#0B1124',height:'100%'}}>
        <BookingHeader/>

        <View style = {styles.container}>
            <Text style = {styles.title}>Book a ride</Text>
            <Text style = {styles.description}>You will get loyalty points in each completed ride which you can
exchange for vouchers or Lori Coins.</Text>
        </View>

        <View style = {[styles.container,{marginTop:20}]}>
          <TextInput
            label="Your Location"
            placeholder='Thori Lam, Thimphu'
            left={<TextInput.Icon icon="map-marker-outline" />}
            style = {styles.textInputStyle}
          />

          <TextInput
            label="Drop point"
            left={<TextInput.Icon icon="map-marker-outline" />}
            style = {styles.textInputStyle}
          />

          <TouchableOpacity 
            style = {styles.textInputStyle}
            onPress={showDatePicker}
          >
            <TextInput
              label="Travel date"
              value={selectedDate ? selectedDate.toLocaleDateString('en-US'):''}
              left={<TextInput.Icon icon="calendar" />}
              editable = {false}
            />
          </TouchableOpacity>

          <DateTimePickerModal
            isVisible = {isDatePickerVisible}
            mode='date'
            onConfirm={handleDateConfirm}
            onCancel={hideDatePicker}
          />

          <TouchableOpacity
            style = {styles.textInputStyle}
            onPress={showTimePicker}
          >
            <TextInput
              label="Travel time"
              value={selectedTime?formatTime(selectedTime):''}
              left={<TextInput.Icon icon="clock-time-five-outline" />}
              editable = {false}
            />
          </TouchableOpacity>

          <DateTimePickerModal
            isVisible = {isTimePickerVisible}
            mode='time'
            onConfirm={handleTimeConfirm}
            onCancel={hideTimePicker}
          />

          <View style = {styles.radiobuttonlabelContainer}>
            <Text style = {{color:'white'}}>I will travel now</Text>
            <RadioButton
              value="second"
              status="unchecked"
              color='white'
              uncheckedColor='white'
            />
          </View>

          <Button mode="contained"
            textColor='black'
            theme={{ colors: { primary: '#15FF8A' } }}
            onPress={() => {
              navigation.navigate('AvailableRides');
            }}
            style = {{borderRadius:5,width:'80%',height:50,justifyContent:'center',marginTop:10}}
          >
            See Available Rides
          </Button>
        </View>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal:10
    },
   title: {
    color:'white',
    fontSize:24,
    marginTop:50
   },
   description: {
    color:'#8694A1',
    textAlign:'center',
    marginTop:5
   },
   textInputStyle: {
    width:'80%',
    borderRadius:5,
    marginTop:10
   },
   radiobuttonlabelContainer: {
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-end',
    width:'80%',
    marginTop:10
   }
})

export default BookRide