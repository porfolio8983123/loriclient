import { View, Text, StyleSheet } from 'react-native'
import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';


const BookingHeader = () => {
  return (
    <View style = {styles.container}>
      <View style = {styles.backButton}>
        <Icon name='leftcircleo' size={30} color="white"/>
        <Text style = {styles.title}>Back</Text>
      </View>
      <View>
        <View style = {styles.outerstar}>
            <Icon name='star' color="#1A1A1A" size={22} style = {styles.starIcon}/>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        alignItems:'center',
        marginTop:20,
        justifyContent:'space-between',
        marginHorizontal:20
    },
    title: {
        color:'white',
        fontSize:16,
        marginLeft:5
    },
    backButton: {
        flexDirection:'row',
        alignItems:'center',
    },
    outerstar: {
        backgroundColor:'#15FF8A',
        borderRadius:50
    },
    starIcon: {
        padding:7
    }
})

export default BookingHeader